package permutations
import "testing"
import "fmt"

func setupSamplePermutor() *Permutor {
  perm := NewPermutor(func(l, r int) bool {return l<r})

  perm.AddValue(1)
  perm.AddValue(2)
  perm.AddValue(3)
  perm.AddValue(4)
  perm.AddValue(5)

  return perm
}

var FirstPerm = []int{1, 2, 3, 4, 5}
var SecondPerm= []int{1, 2, 3, 5, 4}
var LastPerm  = []int{5, 4, 3, 2, 1}

func TestValueFunctions(t *testing.T) {
  p := setupSamplePermutor()

  if !p.HasValue(3) || p.HasValue(6) {
    t.Errorf("hasValue failed to report correct values for 3, and 6 in set [1, 2, 3, 4, 5]")
  } else {
    fmt.Println("[+] HasValue detects included and excluded elements")
  }

  if !isSameContents(p.GetValues(), []int{1, 2, 3, 4, 5}) {
    t.Errorf("GetValues returned bad set: %v", p.GetValues())
  } else {
    fmt.Println("[+] GetValues returned proper set")
  }
}

func TestPermuteFunctions(t *testing.T){
  p := setupSamplePermutor()

  first := p.GetFirst()
  last   := p.GetLast()
  second := p.GetNext()

  if !p.IsValidPermutation([]int{2, 3, 5, 4, 1}) {
    t.Errorf("IsValidPermutation did not recognize [2, 3, 5, 4, 1] as a valid permutation of [1, 2, 3, 4, 5]")
  } else {
    fmt.Println("[+] IsValidPermutation correct behaviour verified")
  }

  if p.IsValidPermutation([]int{1, 1, 1, 1, 1}) {
    t.Errorf("IsValidPermutation did not catch duplicates in [1, 1, 1, 1, 1]")
  } else {
    fmt.Println("[+] IsValidPermutation caught duplicates")
  }

  if p.IsValidPermutation([]int{1, 2, 3, 4, 6}) {
    t.Errorf("IsValidPermutation did not catch 6 as a value outside of set [1, 2, 3, 4, 5]")
  } else {
    fmt.Println("[+] IsValidPermutation caught external element in candidate")
  }

  if !isEqSlices(first, FirstPerm) {
    t.Errorf("GetFirst error: %v != %v", first, FirstPerm)
  } else {
    fmt.Println("[+] GetFirst returns first commbination of elements")
  }

  if !isEqSlices(last, LastPerm) {
    t.Errorf("GetLast error: %v != %v", last, LastPerm)
  } else {
    fmt.Println("[+] GetLast returns last combination")
  }

  if !isEqSlices(second, SecondPerm) {
    t.Errorf("GetNext error: %v != %v", second, SecondPerm)
  } else {
    fmt.Println("[+] GetNext returns second permutation")
  }

  if !p.HasMorePermutations() {
    t.Errorf("HasMorePermutations does not see any permutations to be had after [1, 2, 3, 5, 4]")
  } else {
    fmt.Println("[+] HasMorePermutations correct behaviour verified")
  }

  p.SetSeed([]int{5, 4, 3, 2, 1})
  if p.HasMorePermutations() {
    t.Errorf("HasMorePermutations returned true on [5, 4, 3, 2, 1]")
  } else {
    fmt.Println("[+] HasMorePermutations caught lack of perms")
  }
}

func TestPermutationAccuracy(t *testing.T){
  p := setupSamplePermutor()
  permSet := getAllPerms(p)

  if len(permSet) != fac(5) {
    t.Errorf("Permutor did not make 120 permutations, it made: %d", len(permSet))
  }

  if hasDuplicates(permSet) {
    t.Errorf("Permutor output duplicate permutations")
  }

  for _, i := range permSet {
    if !p.IsValidPermutation(i) {
      t.Errorf("Permutor put out invalid permutation: %v", i)
    }
  }
}

func BenchmarkPermutor(b *testing.B) {
  for i := 0; i < b.N; i++ {
    p := setupSamplePermutor()
    getAllPerms(p)
  }
}

//benchmark get all perms of 1-5

// UTILITIES
func fac(n int) int {
  if n < 0 {
    return 0
  }

  if n == 1 {
    return 1
  } else {
    return n * fac(n - 1)
  }
}

func getAllPerms(p *Permutor) [][]int {
  permSet := make([][]int, fac(5))

  permSet[0] = p.GetCurrent()
  for i := 1; p.HasMorePermutations(); i++ {
    permSet[i] = p.GetNext()
  }

  return permSet
}

func hasDuplicates(arg [][]int) bool {
  var set [][]int
  for _, value := range arg {
    for _, exists := range arg {
      if isEqSlices(value, exists) {
        return false
      } else {
        set = append(set, value)
      }
    }
  }

  return true
}

func isEqSlices(left []int, right []int) bool {
  if len(left) != len(right) {
    return false
  }

  for i := 0; i < len(left); i++ {
    if left[i] != right[i] {
      return false
    }
  }

  return true
}

