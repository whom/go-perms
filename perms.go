
package permutations
import "sort"

// This is written to handle integers, which can be used as indexes in a vector of ambiguous data
type Permutor struct {
  // set of elements in the space to be permuted
  set map[int]bool

  // lexical ordering funcion, equivalient to "less than".
  // Ideally, one could pass in a function that takes the int arguments, and orders them by index into an array.
  // IE: should return left < right
  lt func(left int, right int) (bool)

  // internal state, ie: "current" permutation
  seed *[]int
}

// stuff for using sort.Sort
type By func(i int, j int) bool
func (by By) Sort(arg []int) {
  i := &intSliceSorter{
    sl: arg,
    lt: by,
  }

  sort.Sort(i)
}

type intSliceSorter struct {
  sl []int
  lt By
}

func (iss *intSliceSorter) Len() int { return len(iss.sl) }
func (iss *intSliceSorter) Swap(i, j int) { iss.sl[i], iss.sl[j] = iss.sl[j], iss.sl[i] }
func (iss *intSliceSorter) Less(i, j int) bool { return iss.lt(iss.sl[i], iss.sl[j]) }

// reset seed on permutor, called by AddValue()
func (p Permutor) reseed() {
  if len(p.set) < 1 {
    return
  }

  (*p.seed) = append([]int(nil), p.GetFirst()...)
}

func NewPermutor(cmp func(a int, b int)(bool)) *Permutor {
  p := new(Permutor)
  p.set = make(map[int]bool)
  p.lt = cmp
  p.seed = new([]int)
  return p
}

// performs lexicographic incrementation
func (p Permutor) permute() {
  // any occurence of a single item set was handled in p.GetNext before this was called.
  // at this time we know there are new permutations to be had
  if len(p.set) == 2 {
    temp := (*p.seed)[0]
    (*p.seed)[0] = (*p.seed)[1]
    (*p.seed)[1] = temp
  }

  var leftSwapCandidate = -1
  var rightSwapCandidate = -1

  // find where the numbers start decreasing from the right
  last := len(*p.seed)-1
  for i:= len(*p.seed)-2; i > -1; i-- {

    if p.lt((*p.seed)[i], (*p.seed)[last]) {
      leftSwapCandidate = i
      break
    }

    last = i
  }

  // get right swap candidate
  rightSwapCandidate = p.GetLast()[0]
  for i := leftSwapCandidate +1; i < len(*p.seed); i++ {
    val := (*p.seed)[i]

    if val > (*p.seed)[leftSwapCandidate] && val < rightSwapCandidate {
      rightSwapCandidate = val

      if val == rightSwapCandidate+1 {
        break
      }
    }
  }

  rightSwapCandidate = pos(*p.seed, rightSwapCandidate)


  temp := (*p.seed)[leftSwapCandidate] // FUCK
  (*p.seed)[leftSwapCandidate] = (*p.seed)[rightSwapCandidate]
  (*p.seed)[rightSwapCandidate] = temp

  // reverse the tail
  s := (*p.seed)[leftSwapCandidate+1:]
  s = reverse(s)
  (*p.seed) = append((*p.seed)[:leftSwapCandidate+1], s...)
}

// Warning, addValue and removeValue reset the seed
func (p Permutor) AddValue(i int) {
  p.set[i] = true
  p.reseed()
}

func (p Permutor) RemoveValue(i int) {
  if !p.HasValue(i) {
    return
  }

  delete(p.set, i)
  p.reseed()
}

func (p Permutor) HasValue(i int) bool {
  _, present := p.set[i]
  return present
}

// returned data is not sorted
// if you want sorted data just get initial perm
// or I guess you could reverse the last perm
func (p Permutor) GetValues() []int {
  if len(p.set) < 1 {
    return make([]int, 0)
  }

  vals := make([]int, 0)
  for k, _ := range p.set {
    vals = append(vals, k)
  }

  return vals
}

// sets seed iff argument is valid permutation
func (p Permutor) SetSeed(arg []int) {
  if p.IsValidPermutation(arg) {
    (*p.seed) = append([]int(nil), arg...)
  }
}

// returns false if seed is reverse lexical order of set. returns true otherwise
func (p Permutor) HasMorePermutations() bool {
  if len(p.set) < 1 {
    return false
  }

  var last int
  for index, value := range *p.seed {
    if index == 0 {
      last = value
      continue
    }

    if p.lt(last, value){
      return true
    }

    last = value
  }

  return false
}

// Permutation Accessor Functions
func (p Permutor) GetCurrent() []int {
  s := make([]int, len(*p.seed))
  copy(s, *p.seed)

  return s
}

// gets smallest lexicographically ordered value
func (p Permutor) GetFirst() []int {
  s := append([]int(nil), p.GetValues()...)

  By(p.lt).Sort(s)
  return s
}

func (p Permutor) GetLast() []int {
  s := make([]int, len(p.set))
  copy(s, p.GetValues())

  By(func(a int, b int) bool { return !p.lt(a, b)}).Sort(s)
  return s
}

// permutes, returns next permutation
func (p Permutor) GetNext() []int {
  if !p.HasMorePermutations() {
    return []int{}
  }

  p.permute()
  return p.GetCurrent()
}

// returns true if arg has no duplicate elements and all elements in arg are also in p.set
func (p Permutor) IsValidPermutation(arg []int) bool {
  s := make([]int, len(arg))

  for index, value := range arg {
    for _, exists := range s {
      if value == exists {
        return false
      }
    }

    s[index] = value
  }

  if !isSameContents(arg, p.GetValues()) {
    return false
  }

  return true
}

// UTILITY FUNCTIONS ===============================
func isSameContents(left []int, right []int) bool {
  if len(left) != len(right) {
    return false
  }

  found := false

  for _, l := range left {
    for _, r := range right {
      if l == r {
        found = true
      }
    }

    if !found {
      return false
    }

    found = false
  }

  return true
}

func pos(sl []int, arg int) int {
  for i, v := range sl {
    if (v == arg) {
      return i
    }
  }

  return -1
}

func reverse(arg []int) []int {
  s := []int(nil)
  for i := len(arg)-1; i > -1; i-- {
    s = append(s, arg[i])
  }

  return s
}
