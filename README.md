# go-perms
A library for permuting finite sets of elements

## How to use
- Import "gitlab.com/whom/go-perms"
- Create a method to compare two elements, taking indexes for either element
  - method should return element1 < element2
- Construct a new permutor
  - permutations.NewPermutor(comparisonFunction)
- Add index values for each element in your set
  - permutor.AddValue(int)
- optional: set starting permutation to sequence of indexes
  - permutor.SetSeed([]int)

## Example Code
```go
package main

import (
  "gitlab.com/whom/go-perms"
  "fmt"
)

func main() {

  // Simple integer permutations
  p := permutations.NewPermutor(func(l int, r int)bool{return l<r})
  p.AddValue(1)
  p.AddValue(2)
  p.AddValue(3)
  p.AddValue(4)

  permCounter := 0
  for i := p.GetCurrent(); p.HasMorePermutations(); i = p.GetNext() {
    fmt.Println("[+] ", i)
    permCounter++
  }

  fmt.Println("[+] ",p.GetLast())
  fmt.Println(permCounter+1, " total permutations")

}
```

## Contributing
Yes please.
